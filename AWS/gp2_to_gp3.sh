#! /bin/bash


all_white_instances_ids=( "$@" ) #gets all instances which are in running state

for instance_id in ${all_white_instances_ids[@]}

do
echo "for instance_id: $instance_id"
for vol_id in $(aws ec2 describe-instances --instance-id $instance_id --query "Reservations[*].Instances[*].BlockDeviceMappings[*].Ebs.VolumeId" --output text) #gets all attched volumes of perticular instance
do
volume_type=$(aws ec2 describe-volumes --filter Name=volume-id,Values=$vol_id --query Volumes[*].VolumeType --output text) #gets volume type

if [ $volume_type == "gp2" ]
then
echo " changing to gp3 ...."
echo "for vol-id: $vol_id "
echo $(aws ec2 modify-volume --volume-id $vol_id --volume-type gp3)
echo "______________"
fi

done
done