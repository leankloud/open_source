#! /bin/bash 

black_list=(i-06b27b78d7dd7537d i-00e2db1e2c3517e2d)

instances_ids=$(aws ec2 describe-instances --filters Name=instance-state-name,Values=running --query "Reservations[*].Instances[*].InstanceId" --output text)

count=0
flag=0
for instance_id in ${instances_ids[@]}
do

for black_id in ${black_list[@]}
do
if [ $black_id == $instance_id ]
then
flag=1
break
fi

done
if (( flag == 0 ))
then
white_list[$count]=$instance_id
(( count = count + 1 ))
fi
flag=0
done
echo ${white_list[@]}

./gp2_to_gp3.sh ${white_list[@]}