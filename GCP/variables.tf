variable "credentials_file_path" {
  description = "Path to the GCP credentials JSON file"
  type        = string
}

variable "api_project_ids" {
  description = "List of GCP project IDs where APIs will be enabled (All Targeted Project IDs in [])"
  type        = list(string)
}

variable "service_account_project_id" {
  description = "The GCP project ID where the service account and custom role will be created (Main Project ID)"
  type        = string
}

variable "target_project_ids" {
  description = "List of GCP project IDs where the custom role and IAM bindings will be created (All Targeted Project IDs in [], except main Project ID)"
  type        = list(string)
}