variable "api_project_id" {
  description = "List of GCP project IDs where APIs will be enabled (All Targeted Project IDs in [])"
  type        = list(string)
}

variable "service_account_project_id" {
  description = "The GCP project ID where the service account and custom role will be created (Main Project ID)"
  type        = string
}
