# Provider Block

provider "google" {
  credentials = file(var.credentials_file_path) # Uses variable for the credentials file path
}

/*
Section 1 -> Enable the required APIs in the targeted projects.
*/


# Variable for APIs
variable "apis" {
  description = "List of APIs to enable"
  type        = list(string)
  default     = [
    "sqladmin.googleapis.com",
    "monitoring.googleapis.com",
    "compute.googleapis.com",
    "logging.googleapis.com",
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "sql-component.googleapis.com"
  ]
}

resource "google_project_service" "enable_apis" {
  for_each = { for combo in flatten([for project in var.api_project_ids : [for api in var.apis : { project = project, service = api }]]) : "${combo.project}-${combo.service}" => combo }
  project = each.value.project
  service = each.value.service
  disable_on_destroy = false
}

/*
Section 2 -> Create a service account for Project1,
             define a custom role with the required permissions,
             and attach the role to the service account.
*/

# Create the Service Account
resource "google_service_account" "service_account" {
  project      = var.service_account_project_id # Uses variable for the project ID where the SA is created
  account_id   = "lk-service-account"
  display_name = "LK Service Account"
}

# Create a custom role
resource "google_project_iam_custom_role" "custom_role" {
  project     = var.service_account_project_id
  role_id     = "LKCustomRole"
  title       = "LK Custom Role"
  description = "Custom role to allow viewing and listing Cloud SQL and Compute instances"

  permissions = [
    "compute.instances.get",
    "compute.instances.list",
    "compute.disks.list",
    "compute.disks.get",
    "compute.regions.get",
    "compute.regions.list",
    "compute.zones.get",
    "compute.zones.list",
    "compute.machineTypes.get",
    "compute.machineTypes.list",
    "compute.licenses.list",
    "compute.licenses.get",
    "compute.licenseCodes.list",
    "compute.licenseCodes.get",
    "cloudsql.instances.list",
    "cloudsql.instances.get",
    "cloudsql.databases.get",
    "cloudsql.databases.list",
    "logging.logEntries.list",
    "monitoring.timeSeries.list"
  ]
}

# Grant the custom role to the Service Account
resource "google_project_iam_member" "assign_custom_role" {
  project = var.service_account_project_id
  role    = google_project_iam_custom_role.custom_role.name
  member  = "serviceAccount:${google_service_account.service_account.email}"
}

/*
Section 3 -> Create a custom role in the targeted projects and
             assign the service account created in Project1
             to the other projects with this custom role attached.
*/

# Create a custom role in Target Projects
resource "google_project_iam_custom_role" "target_custom_roles" {
  for_each = toset(var.target_project_ids)
  project  = each.value
  role_id  = "LKCustomRole"
  title    = "LK Custom Role"
  description = "Custom role to allow viewing and listing Cloud SQL and Compute instances"

  permissions = [
    "compute.instances.get",
    "compute.instances.list",
    "compute.disks.list",
    "compute.disks.get",
    "compute.regions.get",
    "compute.regions.list",
    "compute.zones.get",
    "compute.zones.list",
    "compute.machineTypes.get",
    "compute.machineTypes.list",
    "compute.licenses.list",
    "compute.licenses.get",
    "compute.licenseCodes.list",
    "compute.licenseCodes.get",
    "cloudsql.instances.list",
    "cloudsql.instances.get",
    "cloudsql.databases.get",
    "cloudsql.databases.list",
    "logging.logEntries.list",
    "monitoring.timeSeries.list"
  ]
}

resource "google_project_iam_member" "assign_custom_roles" {
  for_each = toset(var.target_project_ids)

  project = each.value
  role    = google_project_iam_custom_role.target_custom_roles[each.key].name
  member  = "serviceAccount:${google_service_account.service_account.email}"
}