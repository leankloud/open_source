#!/bin/bash

# Set paths and variables
PROJECT_DIR="Leankloud-Onboarding-Project"
MAIN_TF_URL="https://bitbucket.org/leankloud/open_source/raw/86aac6f945ef1bbbb181b701fabbecb4a0c68eeb/GCP/main.tf"
VARIABLES_TF_URL="https://bitbucket.org/leankloud/open_source/raw/86aac6f945ef1bbbb181b701fabbecb4a0c68eeb/GCP/variables.tf"
TFVARS_FILE="terraform.tfvars"

# Create the project directory and navigate to it
echo "Setting up the project directory..."
mkdir -p "$PROJECT_DIR"
cd "$PROJECT_DIR" || exit

# Download Terraform configuration files
echo "Downloading Terraform configuration files..."
wget -q "$MAIN_TF_URL" -O main.tf
wget -q "$VARIABLES_TF_URL" -O variables.tf

# Generate a terraform.tfvars file for variable values
echo "Creating terraform.tfvars file..."
cat > "$TFVARS_FILE" <<EOF
credentials_file_path = "/path/to your/key.json" 		# Replace with the organization level sa key file path		
api_project_ids       = ["Project1", "Project2", "Project3", "Project4"]	# Replace with all the targeted Project IDs
service_account_project_id = "Project1"			# Replace with the main Project ID
target_project_ids    = ["Project2", "Project3", "Project4"]		# Replace with all the targeted Project IDs except the main Project ID
EOF

# Initialize Terraform
echo "Initializing Terraform..."
terraform init

# Run Terraform plan
echo "Running Terraform plan..."
terraform plan -var-file="$TFVARS_FILE"

# Apply Terraform configuration
echo "Applying Terraform configuration..."
terraform apply -var-file="$TFVARS_FILE" -auto-approve

echo "Terraform setup and deployment complete!"
