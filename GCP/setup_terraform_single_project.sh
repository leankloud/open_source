#!/bin/bash

# Set paths and variables
PROJECT_DIR="Leankloud-Onboarding-Project"
MAIN_TF_URL="https://bitbucket.org/leankloud/open_source/raw/b0c53e978253532876cd7d1e399624e1edf4f99d/GCP/main_single_project.tf"
VARIABLES_TF_URL="https://bitbucket.org/leankloud/open_source/raw/864dd87c69484539ad3004e31d07a9f412cdf1f4/GCP/variables_single_project.tf"
TFVARS_FILE="terraform.tfvars"

# Create the project directory and navigate to it
echo "Setting up the project directory..."
mkdir -p "$PROJECT_DIR"
cd "$PROJECT_DIR" || exit

# Download Terraform configuration files
echo "Downloading Terraform configuration files..."
wget -q "$MAIN_TF_URL" -O main_single_project.tf
wget -q "$VARIABLES_TF_URL" -O variables_single_project.tf

# Generate a terraform.tfvars file for variable values
echo "Creating terraform.tfvars file..."
cat > "$TFVARS_FILE" <<EOF	
api_project_id       = ["Project1"]	# Replace with the targeted Project ID
service_account_project_id = "Project1"   # Replace with the targeted Project ID
EOF

# Initialize Terraform
echo "Initializing Terraform..."
terraform init

# Run Terraform plan
echo "Running Terraform plan..."
terraform plan -var-file="$TFVARS_FILE"

# Apply Terraform configuration
echo "Applying Terraform configuration..."
terraform apply -var-file="$TFVARS_FILE" -auto-approve

echo "Terraform setup and deployment complete!"
