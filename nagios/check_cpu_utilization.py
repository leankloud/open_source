#!/usr/bin/python
import os
import re
import sys
from collections import defaultdict

# Set variables
COMMAND = "sar 1 300"


def get_cpu_usage():
    try:
        # get cpu utilization
        cpu = os.popen(COMMAND)
        # split out the stats %
        cpu_stats = []
        for data in [i for i in cpu]:
            cpu_stats.append(re.split('\s+', data))
        # Slicing the list to remove unnecessary text
        cpu_stats = cpu_stats[2:]
        # Creating a dictionary to store all stats as key and all their respective values in a list as value
        d = defaultdict(list)
        for i in range(1, len(cpu_stats)-1):
            for j in range(0, len(cpu_stats[0])):
                d[cpu_stats[0][j]].append(float(cpu_stats[i][j])) if re.search('^%', cpu_stats[0][j]) else d[cpu_stats[0][j]].append(cpu_stats[i][j])
        maxm = max(d['%user'])
        avg = sum(d['%user'])/len(d['%user'])
    except:
        print("STATE UNKNOWN")
        sys.exit(3)

    e = dict()
    e['Maximum'] = "%.2f" % maxm
    e['Average'] = "%.2f" % avg
    e['Unit'] = "Percent"
    print(e)
    sys.exit(0)


if __name__ == '__main__':
    get_cpu_usage()
